HubShark CMS
============

A Symfony project created to serve as a complete blogging platform.


## Features

* Admin Area
* User Login
* User Registration
* EU COOKIE LAW Conform
* Static Type Pages with Administration
* Blog Posts with Administration
* International (i18n) - German and English included
* Contact Page
* Site Search
* Tagging System
* Comment System
* Easy to Modify


## License

The MIT License (MIT) - [see the details here](https://gitlab.com/HubShark/hubshark-cms/blob/master/LICENSE)


## Documentation

In the [wiki pages](https://gitlab.com/HubShark/hubshark-cms/wikis/home) we have a tutorial on how HubShark was built. At this point in development, that will be the _**ONLY**_ docs available.


#### How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/HubShark/hubshark-cms.git`

**IMPORTANT!!!** Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd hubshark`

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
 
# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an [Online Generated key here](http://nux.net/secret).

`composer install`

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

`php bin/console doctrine:database:create`

And then to make sure we got erverything the first time, we run the _install_ command again:

`composer install`


Then Create the database schema

`php bin/console doctrine:schema:create`


And now update your database:

`php bin/console doctrine:schema:update --force`


Then create your :Admin User_ (Replace _"Admin"_ with your adminuser name):

`php bin/console fos:user:create Admin --super-admin`


And then we run this to update everything:

`composer update --with-dependencies`


_I HAVE NOT TESTED THIS YET. If you do please post an issue and let us know._


#### Looks

We have a combination of _Twitter Bootstrap, JQuery and Twig_ setup so our basic site will look something like this:

![Looks Like This](imgz/Screenshot at 2016-12-08 15:46:36.png)
![Looks-Like-This](http://www.hubshark.com/wp-content/uploads/2016/12/Screenshot-at-2016-12-08-154636.png)
