<?php
// src/AppBundle/Form/ArticleType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ArticleType extends AbstractType {
	
    public function buildForm(FormBuilderInterface $builder, array $options) {
    	
        $builder
            ->add('articleTitle', TextType::class)
            ->add('articleDescription', CKEditorType::class)
        	->add('articleContent', CKEditorType::class)        
            ->add('articleCreationDate', DateType::class)
            ->add('articlePublished', ChoiceType::class, 
            		array(
            			'choices' => array(
            					'1' => $options['yes'],
            					'0' => $options['no']
            		),
            		'expanded' => true,
            		'multiple' => false
            	)
            )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
        	array(
            	'data_class' => 'AppBundle\Entity\Article',
        		'yes' => null,
        		'no' => null
        	)
        );
    }

    public function getBlockPrefix()
    {
        return 'article';
    }
}
